<?php

use Illuminate\Routing\Router;
/** @var Router $router */

$router->group(['prefix' =>'/product'], function (Router $router) {
    $router->get('products', [
        'as' => 'admin.product.product.index',
        'uses' => 'ProductController@index',
        'middleware' => 'can:product.product.index',
    ]);
    $router->get('get-list','ProductController@getList');
    $router->post('create-product','ProductController@createProduct');
});
