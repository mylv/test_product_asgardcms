<?php

use Illuminate\Routing\Router;

/** @var Router $router */
$router->group(['prefix' => '/product', 'middleware' => ['api.token', 'auth.admin']], function (Router $router) {
    
    $router->group(['prefix' => 'products'], function (Router $router) {
        $router->get('/', [
            'as' => 'api.product.listproduct',
            'uses' => 'ProductController@getList',
        ]);
    });

});
