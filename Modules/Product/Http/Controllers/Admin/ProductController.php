<?php

namespace Modules\Product\Http\Controllers\Admin;
use Modules\Core\Http\Controllers\Admin\AdminBaseController;
use Illuminate\Http\Response;
use Modules\Product\Entities\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;

class ProductController extends AdminBaseController
{

    public function index(){
        return view('product::index');
    }

    /*
    Get list ptoduct
    */
    public function getList(){
        return Product::getList([]);
    }

    /*
    *Create product
    *@Param name,price,image,description
    */
    public function createProduct(Request $request){

        $response = [];
        $validator = Validator::make($request->all(),
        [
            'name' => 'required|max:255',
            'price' => 'required|integer',
        ]);
        if ($validator->fails()) {
            $response['success'] = false;
            $response['errors'] = $validator->errors()->all();
            return $response;
        }
        $filename = '';
        if (!empty($request->file('image')) && 
         !empty($request->file('image')->getClientOriginalExtension())) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $imageExtensions = ['jpg', 'jpeg', 'gif', 'png'];
            if(in_array($extension, $imageExtensions))
            {
                $dir = 'images/product/';
                $filename = uniqid() . '_' . time() . '.' . $extension;
                $request->file('image')->move($dir, $filename); 
            }else
            {
                $response['success'] = false;
                $response['errors'][0] = 'Extension file incorrect!';
                return $response;
            }
        }
       
        $data_create = [
            'name' => $request->name,
            'price' => $request->price,
            'image' => $filename,
            'description' => $request->description
        ];
        $create = Product::create($data_create);
        if ($create) {
            $response['success'] = true;
        } else {
            $response['success'] = false;
            $response['errors'][0] = 'Cant create!';
        }
        return $response;
    }
}
