<?php

namespace Modules\Product\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Product\Entities\Product;

class ProductController extends Controller
{


    public function getList(Request $request)
    {
        $data = $request->all();
        $products = Product::getList($data);

        return response()->json([
            'errors' => false,
            'data' => $products,
            'message' => trans('user::messages.user updated'),
        ]);
    }


}
