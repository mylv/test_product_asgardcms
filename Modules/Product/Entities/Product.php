<?php

namespace Modules\Product\Entities;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';
    protected $fillable = [
        'id',
        'name',
        'price',
        'description',
        'image',
        'created_at',
    ];
    private static $limit_per_page = 10;
    
    public static function getList($data = []){
        $products = self::paginate(self::$limit_per_page)->toArray();
        return $products;
    }
}
