<?php namespace Modules\Product\Sidebar;

use Maatwebsite\Sidebar\Badge;
use Maatwebsite\Sidebar\Group;
use Maatwebsite\Sidebar\Item;
use Maatwebsite\Sidebar\Menu;
use Modules\User\Contracts\Authentication;
use Modules\Page\Repositories\PageRepository;

class SidebarExtender implements \Maatwebsite\Sidebar\SidebarExtender
{
    /**
     * @var Authentication
     */
    protected $auth;

    /**
     * @param Authentication $auth
     *
     * @internal param Guard $guard
     */
    public function __construct(Authentication $auth)
    {
        $this->auth = $auth;
    }

    /**
     * @param Menu $menu
     *
     * @return Menu
     */
    public function extendWith(Menu $menu)
    {
        $menu->group(trans('core::sidebar.content'), function (Group $group) {
            $group->item(trans('product::view.Product'), function (Item $item) {
                $item->icon('icon fa fa-product-hunt');
                $item->weight(1);
                $item->route('admin.product.product.index');
                $item->badge(function (Badge $badge) {
                    $badge->setClass('bg-green');
                });
                $item->authorize(
                    $this->auth->hasAccess('product.product.index')
                );
            });
        });

        return $menu;
    }
}
